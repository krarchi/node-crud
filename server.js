const express = require('express');
const bodyParser = require('body-parser');
const mongoClient = require('mongodb').MongoClient
const app = express();

const connectionString = 'mongodb+srv://admin:admin1234@cluster0.ujm7t.mongodb.net/Crud?retryWrites=true&w=majority'
mongoClient.connect(connectionString, {useUnifiedTopology: true}).then(client => {
    const db = client.db('Crud')
    const collection = db.collection('users');
    app.use(bodyParser.urlencoded({extended: true}));
    app.get('/', function (req, res) {
        res.render('main.ejs')
    });
    app.post('/create', (req, res) => {
        var data  = req.body;
        console.log(data.username);
        collection.findOne({username: data.username}, function(err, document) {
            console.log(err,document);
            if(document === null){
                collection.insertOne(req.body).then(result => {
                    res.redirect('/');
                })
                    .catch(error => {
                        console.log(error)
                    })
            }else{
                console.log('User Present')
                res.redirect('/')
            }
        });

    });
    app.get('/list', (req, res) => {
        collection.find().toArray().then(results =>{
            res.render('index.ejs', {users: results})
        });
    });
    app.get('/update/:id', (req,res) => {
        console.log(req.params.id)
        collection.findOne({username: req.params.id}, function(err, document) {
            res.render('update.ejs', {user: document})
        });
    });
    app.use(bodyParser.urlencoded({extended: true}));
    app.post('/modify', (req, res) => {
        console.log(req.body);
        collection.findOneAndUpdate(
            { username: req.body.username },
            {
                $set: {
                    name: req.body.name,
                    username: req.body.username,
                }
            },
            {
                upsert: true
            }
        )
            .then(result => {console.log('Updated');
                res.redirect('/list');
            })
            .catch(error => console.error(error))
    });
    app.get('/delete/:username', (req,res) =>{
        collection.remove({
            username: req.params.username
        }).then(result => {
            res.redirect('/list');
        })
    });
    app.get('/search/:text',(req,res) => {
        console.log(req.params.text)
        collection.find({
            $or: [
                {name: req.params.text},
                {username: req.params.text}
            ]
        }).toArray().then(results => {
            console.log(results);
            if (results.length === 0){
                collection.find().toArray().then(results =>{
                    res.render('index.ejs', {users: results})
                });
            }else{
                console.log(results);
                res.render('index.ejs', {users: results})
            }
        })
    })
});

app.listen(process.env.PORT || 3000, function(err){
    if (err) console.log(err);
    console.log("Server listening on PORT 3000");
});
